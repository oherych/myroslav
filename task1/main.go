package main

import (
	"fmt"

	"sync"

	"bitbucket.org/oherych/myroslav/task1/inc"
)

const (
	numbers   = 4
	range_min = -100
	range_max = 100
)

func main() {
	var wg sync.WaitGroup

	pipeline := inc.NewPipeline(inc.PipeSqrt{}, inc.PipePow{})

	go func() {
		pipeline.Dequeue(func(block inc.Block) {
			if block.Base != block.Value {
				fmt.Println(block.Base, block.Value)
			}
			wg.Done()
		})
	}()

	wg.Add(numbers)
	for j := 0; j < numbers; j++ {
		pipeline.Enqueue(inc.Block{
			Base: inc.RangeRandom(range_min, range_max),
		})
	}

	wg.Wait()
}
