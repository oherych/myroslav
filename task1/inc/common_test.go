package inc

import "testing"

func TestRangeRandom(t *testing.T) {
	table := []struct {
		Min int
		Max int
	}{
		{Min: -100, Max: 100},
		{Min: -200, Max: -100},
		{Min: 0, Max: 100},
		{Min: 100, Max: 200},
	}

	for _, item := range table {
		res := RangeRandom(item.Min, item.Max)

		if res > float64(item.Max) || res < float64(item.Min) {
			t.Errorf("result %v should be in range (%v, %v)", res, item.Min, item.Max)
		}
	}
}
