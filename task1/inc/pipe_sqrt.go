package inc

import (
	"math"
)

type PipeSqrt struct {
	head chan Block
	tail chan Block
}


func (p PipeSqrt) Process(in chan Block) chan Block {
	out := make(chan Block)
	go func() {
		for block := range in {
			block.Value = math.Sqrt(block.Base)

			out <- block
		}
		close(out)
	}()
	return out
}