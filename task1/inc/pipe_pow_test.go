package inc

import (
	"testing"
)

func TestPipePow_Process(t *testing.T) {
	in := make(chan Block)
	a := PipePow{}
	out := a.Process(in)

	table := map[float64]float64{
		1:  1,
		2:  4,
		-2: 4,
	}

	for val, exp := range table {
		in <- Block{Value: val}
		r := <-out

		if exp != r.Value {
			t.Errorf("exp %v got: %v for value %v", exp, r.Value, val)
		}
	}

}
