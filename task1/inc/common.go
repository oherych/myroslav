package inc

import (
	"math/rand"
	"time"
)

func RangeRandom(min, max int) float64 {
	rand.Seed(int64(time.Now().Nanosecond()))
	return float64(rand.Intn(max-min)+min)
}
