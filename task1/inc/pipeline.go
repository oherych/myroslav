package inc

type Pipeline struct {
	head chan Block
	tail chan Block
}

func (p *Pipeline) Enqueue(item Block) {
	p.head <- item
}

func (p *Pipeline) Dequeue(handler func(block Block)) {
	for block := range p.tail {
		handler(block)
	}
}

func NewPipeline(pipes ...Pipe) Pipeline {
	head := make(chan Block)
	var next_chan chan Block

	for _, pipe := range pipes {
		if next_chan == nil {
			next_chan = pipe.Process(head)
		} else {
			next_chan = pipe.Process(next_chan)
		}
	}

	return Pipeline{head: head, tail: next_chan}
}
