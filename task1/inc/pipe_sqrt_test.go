package inc

import (
	"math"
	"testing"
)

func TestPipeSqrt_Process(t *testing.T) {
	in := make(chan Block)
	a := PipeSqrt{}
	out := a.Process(in)

	table := map[float64]float64{
		1:  1,
		2:  1.4142135623730951,
		4:  2,
		-4: math.NaN(),
	}

	for val, exp := range table {
		in <- Block{Base: val}
		r := <-out

		if exp != r.Value && (!math.IsNaN(exp) || !math.IsNaN(r.Value)) {
			t.Errorf("exp %v got: %v for value %v", exp, r.Value, val)
		}
	}

}
