package inc

type Block struct {
	Base  float64
	Value float64
}

type Pipe interface {
	Process (in chan Block) chan Block
}
