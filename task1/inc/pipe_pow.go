package inc

type PipePow struct {
	In  chan Block
	Out chan Block
}

func (p PipePow) Process (in chan Block) chan Block {
	out := make(chan Block)
	go func() {
		for block := range in {
			block.Value = block.Value * block.Value

			out <- block
		}
		close(out)
	}()
	return out
}
